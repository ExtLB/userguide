const async = require('async');
let log = require('@dosarrest/loopback-component-logger')('mod-@extlb/module-userguide/boot');
class Boot {
  constructor(app, done) {
    let me = this;
    me.app = app;
    me.done = done;
    me.init();
  }
  init() {
    let me = this;
    let app = me.app;
    let done = me.done;
    let registry = app.registry;
    let ACL = registry.getModelByType('ACL');
    let UserGuidePage = registry.getModelByType('userguidePage');

    async.series({
      mainNavigation: (cb) => {
        let MainNavigation = registry.getModelByType('MainNavigation');
        MainNavigation.find({where: {routeId: 'userguide', perspective: 'main'}}).then(navItem => {
          if (navItem.length === 0) {
            MainNavigation.create({
              text: 'mod-userguide:USERGUIDE',
              perspective: 'main',
              iconCls: 'x-fa fa-book',
              // rowCls: 'nav-tree-badge nav-tree-badge-new',
              viewType: 'userguide',
              routeId: 'userguide',
              weight: 3,
              leaf: true,
            }).then(newNavItem => {
              // log.info(newNavItem);
              cb(null, true);
            }).catch(err => {
              cb(err, false);
            });
          } else {
            cb(null, true);
          }
        }).catch(err => {
          cb(err, true);
        });
      },
      adminNavigation: (cb) => {
        let AdminNavigation = registry.getModelByType('AdminNavigation');
        AdminNavigation.find({where: {routeId: 'userguide', perspective: 'admin'}}).then(navItem => {
          if (navItem.length === 0) {
            AdminNavigation.create({
              text: 'mod-userguide:USERGUIDE',
              perspective: 'admin',
              iconCls: 'x-fa fa-book',
              // rowCls: 'nav-tree-badge nav-tree-badge-new',
              viewType: 'userguide',
              routeId: 'userguide',
              weight: 3,
              leaf: true,
            }).then(newNavItem => {
              // log.info(newNavItem);
              cb(null, true);
            }).catch(err => {
              cb(err, false);
            });
          } else {
            cb(null, true);
          }
        }).catch(err => {
          cb(err, true);
        });
      },
      UserGuidePageAdminACL: (cb) => {
        ACL.findOrCreate({
          model: UserGuidePage.definition.name,
          accessType: '*',
          principalType: 'ROLE',
          principalId: 'Administrator',
          permission: 'ALLOW',
          property: '*'
        }).then((acl) => {
          cb(null, true);
        }).catch((err) => {
          log.error(err);
          cb(null, true)
        });
      },
      UserGuidePageEveryoneACL: (cb) => {
        ACL.findOrCreate({
          model: UserGuidePage.definition.name,
          accessType: '*',
          principalType: 'ROLE',
          principalId: '$everyone',
          permission: 'DENY',
          property: '*'
        }).then((acl) => {
          cb(null, true);
        }).catch((err) => {
          log.error(err);
          cb(null, true)
        });
      },
      UserGuidePageUserACL: (cb) => {
        ACL.findOrCreate({
          model: UserGuidePage.definition.name,
          accessType: 'READ',
          principalType: 'ROLE',
          principalId: '$everyone',
          permission: 'ALLOW',
          property: 'find'
        }).then((acl) => {
          cb(null, true);
        }).catch((err) => {
          log.error(err);
          cb(null, true)
        });
      }
    }, (err, menuTasks) => {
      if (err) log.error(err);
      done(null, true);
    });
  }
}
module.exports = Boot;
