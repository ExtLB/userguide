Ext.define('Client.view.main.userguideWindow.ViewController', {
  extend: 'Ext.app.ViewController',
  alias: 'controller.main.userguideWindow',

  onInitialize: function (component, eOpts) {
    let me = this;
    let vm = me.getViewModel();
    vm.set('extraParams', {
      filter: JSON.stringify({where: {page: {like: window.location.hash.substr(1)+'.*'}}})
    });
    vm.set('apiUrl', Client.app.viewport.getActiveItem().getViewModel().get('apiUrl'));
    setTimeout(() => {
      vm.getStore('pages').load();
    }, 10);
  }

});
