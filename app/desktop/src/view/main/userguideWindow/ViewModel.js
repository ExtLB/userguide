Ext.define('Client.view.main.userguideWindow.ViewModel', {
  extend: 'Ext.app.ViewModel',
  alias: 'viewmodel.main.userguideWindow',
  data: {
    user: {},
    extraParams: {}
  },
  stores: {
	  pages: {
	    autoLoad: false,
      pageSize: 10000,
      proxy: {
	      type: 'rest',
        url: '{apiUrl}/userguidePages',
        extraParams: '{extraParams}',
        filterParams: 'filters',
        actionMethods: {
	        create: "POST",
          read: "GET",
          update: "PATCH",
          destroy: "DELETE"
        },
        reader: {
	        type: 'json',
          totalProperty: 'total',
          rootProperty: 'data'
        }
      },
      fields: [
        {
          type: 'string',
          name: 'id'
        },
        {
          type: 'string',
          name: 'title'
        },
        {
          type: 'string',
          name: 'page'
        },
        {
          type: 'string',
          name: 'content'
        }
      ]
    }
  }
});
