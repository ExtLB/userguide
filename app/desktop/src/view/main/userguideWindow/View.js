Ext.define('Client.view.perspective.main.userguideWindow.View', {
  extend: 'Ext.Dialog',
  xtype: 'userguideWindow',
  controller: {type: 'main.userguideWindow'},
  viewModel: {type: 'main.userguideWindow'},
  requires: [
    'Ext.grid.column.Column',
    'Ext.grid.column.Text',
    'Ext.grid.plugin.RowExpander',
    'Ext.grid.Grid',
    'Ext.Dialog'
  ],
  layout: {
    type: 'fit'
  },
  title: 'User Guide',
  height: '50%',
  width: '50%',
  modal: true,
  maximizable: true,
  closable: true,
  listeners: {
    initialize: 'onInitialize'
  },
  items: [
    {
      xtype: 'grid',
      selectable: { mode: 'single' },
      bind: {
        store: '{pages}'
      },
      plugins: [
        {
          type: 'rowexpander'
        },
      ],
      itemConfig: {
        viewModel: true,
        xtype: 'gridrow',
        body: {
          tpl: '{content}'
        }
      },
      columns: [
        {
          text: 'Page',
          dataIndex: 'title',
          flex: 1,
        }
      ]
    }
  ]
});
