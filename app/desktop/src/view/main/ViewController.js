Ext.define('Client.view.userguide.ViewController', {
	extend: 'Ext.app.ViewController',
	alias: 'controller.userguide',

	onItemSelected: function (sender, record) {
		Ext.Msg.confirm('Confirm', 'Are you sure?', 'onConfirm', this);
	},

	onConfirm: function (choice) {
		if (choice === 'yes') {
			//
		}
	}
});
