Ext.define('Client.view.userguide.View',{
	extend: 'Ext.grid.Grid',
	xtype: 'userguide',
  requires: [
    'Ext.grid.plugin.RowExpander'
  ],
	cls: 'userguideview',
	requires: [],
	controller: {type: 'userguide'},
	viewModel: {type: 'userguide'},
  perspectives: ['main'],
	selectable: { mode: 'single' },
  bind: {
	  store: '{pages}'
  },
  plugins: [
    {
      type: 'rowexpander'
    },
  ],
  itemConfig: {
	  viewModel: true,
    xtype: 'gridrow',
    body: {
	    tpl: '{content}'
    }
  },
	columns: [
		{
			text: 'Page',
			dataIndex: 'title',
			flex: 1,
		}
	]
});
