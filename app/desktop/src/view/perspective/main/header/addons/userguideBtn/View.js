Ext.define('Client.view.perspective.main.header.addons.userguideBtn.View', {
  extend: 'Ext.Container',
  controller: {type: 'perspective.main.header.addons.userguideBtn'},
  // viewModel: {type: 'perspective.main.header.addons.userguideBtn'},
  requires: [
    'Ext.grid.column.Column',
    'Ext.grid.column.Text',
    'Ext.grid.plugin.RowExpander',
    'Ext.grid.Grid',
    'Ext.Dialog'
  ],
  padding: 5,
  // listeners: {
  //   initialize: 'onUserGuideBtnInitialize'
  // },
  items: [{
    xtype: 'button',
    ui: 'round raised',
    bind: {
      tooltip: '{"mod-userguide:USERGUIDE":translate}',
    },
    reference: 'userguideBtn',
    handler: 'onUserGuideBtn',
    iconCls: 'x-fa fa-book',
  }]
});
Client.view.perspective.main.header.addons.userguideBtn.View.addStatics({
  order: 2
});
