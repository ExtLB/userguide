Ext.define('Client.view.perspective.main.header.addons.userguideBtn.ViewController', {
  extend: 'Ext.app.ViewController',
  alias: 'controller.perspective.main.header.addons.userguideBtn',

  onUserGuideBtn: (button) => {
    // let Navigation = Client.app.getController('Navigation');
    // Navigation.gotoPerspective('admin');
    let window = Ext.create({
      xtype: 'userguideWindow',
    });
    window.show();
  },

  onUserGuideBtnInitialize: (view) => {
    let me = this;
    let vm = view.getViewModel();
    let Loader = Client.app.getController('Loader');
    if (Loader.serverInfo.auth.enabled === true) {
      let Authentication = Client.app.getController('Authentication');
      if (Authentication.loggedIn === true) {
        vm.set('user', Authentication.user);
        let adminRole = _.find(Authentication.user.roles, (role) => {
          return role.name === 'Administrator';
        });
        vm.set('isAdmin', !_.isUndefined(adminRole));
      }
      Authentication.on('login', () => {
        let user = Authentication.user;
        vm.set('user', user);
        let adminRole = _.find(user.roles, (role) => {
          return role.name === 'Administrator';
        });
        vm.set('isAdmin', !_.isUndefined(adminRole));
      });
    } else {
      vm.set('isAdmin', true);
    }
    // setTimeout(() => {
    //   let perspective = view.up('perspective\\.main');
    //   let mainVM = perspective.getViewModel();
    //   let user = mainVM.get('user');
    //   // let perspective = Ext.Viewport.down('perspective\\.main');
    //   console.log(view, perspective, user);
    // }, 10);
  }
});
