Ext.define('Client.view.admin.userguide.Editor', {
  extend: 'Ext.form.Panel',
  alias: 'widget.admin.userguide.editor',

  requires: [
    'Client.view.admin.userguide.EditorViewModel',
    'Client.view.admin.userguide.EditorViewController',
    'Ext.form.Panel',
    'Ext.field.Text',
    'Ext.Component',
    'Ext.Container'
  ],

  controller: 'admin.userguide.editor',
  viewModel: {
    type: 'admin.userguide.editor'
  },
  listeners: {
    initialize: 'onInitialize'
  },

  items: [
    {
      xtype: 'textfield',
      label: 'Title',
      required: true,
      bind: {
        value: '{record.title}'
      }
    },
    {
      xtype: 'textfield',
      label: 'Page',
      required: true,
      bind: {
        value: '{record.page}'
      }
    },
    {
      xtype: 'component',
      reference: 'contentArea',
    }
  ],

  // items: [
  //   {
  //     xtype: 'container',
  //     reference: 'rowContent',
  //     bind: {
  //       data: '{record}'
  //     },
      // html: 'TESTS GRID ROW',
      // tpl: '<div>Account ID: {accountId}</div>' +
      //   '<div>Account: {account}</div>' +
      //   '<div>Created At: {createdAt}</div>' +
      //   '<div>Started At: {startedAt}</div>' +
      //   '<div>Ended At: {endedAt}</div>' +
      //   '<div>Status: {status}</div>' +
      //   '<div>Progress: {progress}</div>' +
      //   '<div>Options: {options}</div>' +
      //   '<div>Targets: {targets}</div>' +
      //   '<div>Regions: {regions}</div>' +
      //   '<div>Attacks: {attacks}</div>' +
      //   '<div>User ID: {userId}</div>' +
      //   '<div>User: {user}</div>',
  //   }
  // ],

  // widget: ,

  updateRecord: function (record, oldRecord) {
    let me = this;
    let vm = me.getViewModel();
    let editor = me.getController().editor;
    vm.set('record', record);
    if (editor.getData() !== record.get('content')) {
      editor.setData(record.get('content'));
    }
    this.callParent([record, oldRecord]);
  }

});
