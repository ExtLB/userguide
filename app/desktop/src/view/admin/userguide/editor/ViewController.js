Ext.define('Client.view.admin.userguide.EditorViewController', {
  extend: 'Ext.app.ViewController',
  alias: 'controller.admin.userguide.editor',

  onInitialize: function (component, eOpts) {
    let me = this;
    let vm = me.getViewModel();
    let contentArea = me.lookup('contentArea');
    let editable = AlloyEditor.editable(contentArea.bodyElement.dom);
    me.editor = editable._editor;
    me.editor.on('change', () => {
      let value = me.editor.getData();
      vm.get('record').set('content', value);
    });
    // console.log(contentArea.inputElement.id, contentArea.inputElement);
    // setTimeout(() => {
    //   try {
    //     console.log(AlloyEditor.editable(contentArea.inputElement.id));
    //   } catch (err) {
    //     console.log(err);
    //   }
    // }, 100);
  }
});
