Ext.define('Client.view.admin.userguide.ViewModel', {
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.admin.userguide',
	data: {
		name: 'Client'
	},
  stores: {
	  pages: {
	    autoLoad: true,
      pageSize: 10000,
      proxy: {
	      type: 'rest',
        url: '{apiUrl}/userguidePages',
        filterParams: 'filters',
        actionMethods: {
	        create: "POST",
          read: "GET",
          update: "PATCH",
          destroy: "DELETE"
        },
        reader: {
	        type: 'json',
          totalProperty: 'total',
          rootProperty: 'data'
        },
        writer: {
	        type: 'json',
          writeRecordId: false
        }
      },
      fields: [
        {
          type: 'string',
          name: 'id'
        },
        {
          type: 'string',
          name: 'title'
        },
        {
          type: 'string',
          name: 'page'
        },
        {
          type: 'string',
          name: 'content'
        }
      ]
    }
  }
});
