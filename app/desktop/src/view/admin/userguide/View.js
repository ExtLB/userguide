Ext.define('Client.view.admin.userguide.View',{
	extend: 'Ext.grid.Grid',
	xtype: 'admin.userguide',
  requires: [
    'Ext.grid.plugin.RowExpander'
  ],
	cls: 'adminuserguideview',
	requires: [],
	controller: {type: 'admin.userguide'},
	viewModel: {type: 'admin.userguide'},
  perspectives: ['admin'],
	selectable: { mode: 'single' },
  bind: {
	  store: '{pages}'
  },
  plugins: [
    {
      type: 'rowexpander'
    },
  ],
  itemConfig: {
	  viewModel: true,
    xtype: 'gridrow',
    body: {
	    xtype: 'admin.userguide.editor'
    }
  },
	columns: [
		{
			text: 'Title',
			dataIndex: 'title',
			flex: 1,
		},
    {
			text: 'Page',
			dataIndex: 'page',
			flex: 1,
		}
	],
  items: [
    {
      xtype: 'toolbar',
      docked: 'top',
      items: [
        {
          xtype: 'button',
          reference: 'refreshGuide',
          itemId: 'refreshGuide',
          ui: 'raised round',
          iconCls: 'x-fa fa-sync',
          listeners: {
            tap: 'onRefreshGuide'
          }
        },
        {
          xtype: 'button',
          reference: 'addPageBtn',
          itemId: 'addPageBtn',
          ui: 'raised round',
          iconCls: 'x-fa fa-plus htmlcolor-green',
          listeners: {
            tap: 'onAddPage'
          }
        },
        {
          xtype: 'spacer'
        },
        {
          xtype: 'button',
          reference: 'savePages',
          itemId: 'savePages',
          ui: 'raised round',
          iconCls: 'x-fa fa-save htmlcolor-green',
          listeners: {
            tap: 'onSaveGuide'
          }
        }
      ]
    }
  ]
});
