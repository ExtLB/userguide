Ext.define('Client.view.admin.userguide.ViewController', {
	extend: 'Ext.app.ViewController',
	alias: 'controller.admin.userguide',

	onItemSelected: function (sender, record) {
		Ext.Msg.confirm('Confirm', 'Are you sure?', 'onConfirm', this);
	},

	onConfirm: function (choice) {
		if (choice === 'yes') {
			//
		}
	},

  onAddPage: function (button, e, eOpts) {
	  let me = this;
	  let vm = me.getViewModel();
	  let record = vm.getStore('pages').getModel().create({
      title: 'Title',
      page: 'page',
      content: 'Content',
      id: undefined
    });
	  record.set('id', undefined);
	  vm.getStore('pages').add(record);
  },

  onRefreshGuide: function (button, e, eOpts) {
    let me = this;
    let vm = me.getViewModel();
    vm.getStore('pages').load();
  },

  onSaveGuide: function (button, e, eOpts) {
    let me = this;
    let vm = me.getViewModel();
    let hasNewRecords = vm.getStore('pages').getNewRecords().length > 0;
    vm.getStore('pages').sync({
      success: function (batch, options) {
        Ext.toast('User Guide Saved!');
        if (hasNewRecords) {
          vm.getStore('pages').load();
        }
      },
      failure: function (batch, options) {
        Ext.toast('User Guide Not Saved!');
        vm.getStore('pages').load();
      }
    });
  }

});
