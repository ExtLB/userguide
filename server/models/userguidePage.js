"use strict";
exports.__esModule = true;
var log = require('@dosarrest/loopback-component-logger')('capp-controller-attacks/server/models/SavedTest');
var userguidePageModel = (function () {
    function userguidePageModel(model) {
        var me = this;
        me.model = model;
        me.setup();
    }
    userguidePageModel.prototype.setup = function () {
        var me = this;
        return me.setupEvents().then(function (events) {
            return me.setupModel();
        }).then(function (methods) {
            return me.setupMethods();
        })["catch"](function (err) {
            log.error(err);
        });
    };
    userguidePageModel.prototype.setupModel = function () {
        var me = this;
        return new Promise(function (resolve, reject) {
            resolve(true);
        });
    };
    userguidePageModel.prototype.setupEvents = function () {
        var me = this;
        return new Promise(function (resolve, reject) {
            me.model.on('attached', function (app) {
                me.app = app;
                log.info('Attached userguidePage Model to Application');
            });
            resolve(true);
        });
    };
    userguidePageModel.prototype.setupMethods = function () {
        var me = this;
        return new Promise(function (resolve) {
            resolve(true);
        });
    };
    return userguidePageModel;
}());
function default_1(userguidePage) {
    new userguidePageModel(userguidePage);
}
exports["default"] = default_1;
;
//# sourceMappingURL=userguidePage.js.map