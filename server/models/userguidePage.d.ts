import { PersistedModel } from "loopback";
export interface userguidePageInterface extends PersistedModel {
    id: string;
    title: string;
    page: string;
    content: string;
}
export interface userguidePage extends PersistedModel {
}
export default function (userguidePage: userguidePage): void;
