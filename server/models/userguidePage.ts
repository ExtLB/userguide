import {LoopBackApplication, Role, PersistedModel} from "loopback";
let log = require('@dosarrest/loopback-component-logger')('capp-controller-attacks/server/models/SavedTest');

export interface userguidePageInterface extends PersistedModel {
  id: string;
  title: string;
  page: string;
  content: string;
}
export interface userguidePage extends PersistedModel {
}

class userguidePageModel {
  app: LoopBackApplication;
  model: userguidePage;
  constructor(model: userguidePage) {
    let me = this;
    me.model = model;
    me.setup();
  }
  setup() {
    let me = this;
    return me.setupEvents().then((events: any) => {
      return me.setupModel();
    }).then((methods: any) => {
      return me.setupMethods();
    }).catch((err: Error) => {
      log.error(err);
    });
  }
  setupModel() {
    let me = this;
    return new Promise((resolve, reject) => {
      resolve(true);
    });
  }
  setupEvents() {
    let me = this;
    return new Promise((resolve, reject) => {
      (me.model as any).on('attached', (app: LoopBackApplication) => {
        me.app = app;
        log.info('Attached userguidePage Model to Application');
      });
      resolve(true);
    });
  }
  setupMethods() {
    let me = this;
    return new Promise((resolve) => {
      resolve(true);
    });
  }
}

export default function (userguidePage: userguidePage) {
  new userguidePageModel(userguidePage);
};
